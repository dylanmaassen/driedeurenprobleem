void tekenPrijzen(){
  PImage geitImg = loadImage("geit.png");
  PImage autoImg = loadImage("auto.png");
  imageMode(CENTER);
  
  for (int d:deuren){
    int x = xVanDeuren[d]; 
    if(deurenMetGeit.hasValue(d)){
      image(geitImg, x+(deurBreedte/2), 200+(deurHoogte/2));
    } else {
      image(autoImg, x+(deurBreedte/2), 200+(deurHoogte/2));
    }
  
  }

}