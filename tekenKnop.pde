void tekenKnop(String text, int x, int y){
  rectMode(CORNER);
  fill(#0000FF);
  rect(x, y, knopBreedte, knopHoogte);
  textAlign(CENTER, CENTER);
  fill (#FFFFFF);
  text(text, x+(knopBreedte/2), y+(knopHoogte/2));
}