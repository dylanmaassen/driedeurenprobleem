int historieKnopX, historieKnopY,herstartKnopX,herstartKnopY;

int spelStatus = 0;
int aantalDeuren;
int aantalAutos;
int geselecteerdeDeur = -1;
int geselecteerdeDeurMetGeit;
boolean gewonnen;
boolean gewisseld;

int deurHoogte = 400;
int deurBreedte = 200;
int deurMarge = 20;

int [] xVanDeuren = {};

IntList deuren = new IntList();
IntList deurenMetGeit = new IntList();
IntList deurenMetAuto = new IntList();
StringList beurtenGewisseld = new StringList();
StringList beurtenGewonnen = new StringList();

int wisselCounter = 0;
int nietWisselCounter = 0;
int wisselWinCounter = 0;
int nietWisselWinCounter = 0;

int knopHoogte = 50;
int knopBreedte = 200;
int knopMarge = 20;

void setup(){
  size(1600,800);
  historieKnopX = width-knopBreedte-knopMarge;
  historieKnopY = height-70;
  herstartKnopX = width-(knopBreedte*2)-(knopMarge*2);
  herstartKnopY = height-70;
}

void draw(){
  
  background(#FFFFFF);
  tekenKnop("Historie", historieKnopX, historieKnopY);
  tekenScores();
  switch(spelStatus){
    case 0 :
      toonStatus("Kies een aantal deuren (typ 3-7)");
      resetSpel();
      break;
    case 1 :
      toonStatus("Hoeveel auto's zullen we verstoppen? " + "(typ 1-" + (aantalDeuren-2) + ")");
      break;
    case 2:
      toonStatus(aantalDeuren + " deur(en) en " + aantalAutos + " auto(s), akkoord? (j/n)");
      break;
    case 3:
      toonStatus("Kies een deur");
      tekenDeuren();
      break;
    case 4:
      toonStatus("Je hebt deur " + (geselecteerdeDeur+1) + " gekozen, druk op 'o' om verder te gaan");
      tekenDeuren();
      //selecteerDeur();
      break;
    case 5:
      toonStatus("We hebben een deur opengemaakt, wil je van deur wisselen (j/n)");
      tekenPrijzen();
      tekenDeuren();
      break;
    case 6:
      toonStatus("Kies opnieuw een deur:");
      tekenPrijzen();
      tekenDeuren();
      break;
    case 7:
      toonStatus("Het moment van de waarheid, druk op 'o' om je deur te openen");
      tekenPrijzen();
      tekenDeuren();
      break;
    case 8:
      if (gewonnen){
        toonStatus("Gefeliciteerd, deur "  + (geselecteerdeDeur+1) + " is een winnende deur! ([o]pnieuw/[h]istorie)");
      } else {
        toonStatus("Helaas, " + (geselecteerdeDeur+1) + " is geen winnende deur! ([o]pnieuw/[h]istorie)");
      }
      tekenPrijzen();
      tekenDeuren();
      tekenKnop("Opnieuw", herstartKnopX, herstartKnopY);
  };
}

void keyPressed(){
  switch(spelStatus){
    case 0:
      if (key >= '3' && key <= '7'){
        aantalDeuren = int(str(key));
        spelStatus = 1;
      }
      break;
    case 1:
      if (int(str(key)) >= 1 && int(str(key)) <= int((aantalDeuren-2))){
        aantalAutos = int(str(key));
        spelStatus = 2;
      }
      break;
    case 2:
      if(key == 'j'){
        spelStatus = 3;
        berekenDeuren(aantalDeuren, aantalAutos);
      } else if(key == 'n'){
        spelStatus = 0;
      };
      break;
    case 3:
      if(int(str(key)) >= 1 && int(str(key)) <= aantalDeuren){
        spelStatus = 4;
        geselecteerdeDeur = int(str(key))-1;
      }
      break;
    case 4:
      if(key == 'o'){
        spelStatus = 5;
        geselecteerdeDeurMetGeit = selecteerGeit();
      }
      break;
    case 5:
      if(key == 'j'){
        spelStatus = 6;
        gewisseld = true;
      }
      if(key == 'n'){
        spelStatus = 7;
        gewisseld = false;
      }
      break;
    case 6:
      if(int(str(key)) >= 1 && int(str(key)) <= aantalDeuren && int(str(key))-1 != geselecteerdeDeurMetGeit){
        spelStatus = 7;
        geselecteerdeDeur = int(str(key))-1;
      }
      break;
    case 7:
      if(key == 'o'){
        if(deurenMetAuto.hasValue(geselecteerdeDeur)){
         gewonnen = true;
        } else {
          gewonnen = false;
        }
        if (gewisseld == true){
          wisselCounter++;
        } else {
          nietWisselCounter++;
        }
        if (gewonnen == true && gewisseld == true){
          wisselWinCounter++;
        } else if (gewonnen == true && gewisseld == false){
          nietWisselWinCounter++;
        }
        spelStatus = 8;
        beurtenGewonnen.append(str(gewonnen));
        beurtenGewisseld.append(str(gewisseld));
      }
      break;
    case 8:
      if(key == 'o'){
        spelStatus = 0;
      }
      if(key == 'h'){
        printHistorie();
      }
    }
}

void mousePressed(){
  if(muisOverObject(historieKnopX,historieKnopY,knopBreedte, knopHoogte)){
    printHistorie();
  }
  if(spelStatus == 8){
    if(muisOverObject(herstartKnopX,herstartKnopY,knopBreedte, knopHoogte)){
      spelStatus = 0;
    }
  }

}