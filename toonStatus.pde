void toonStatus(String status){
  rectMode(CORNER);
  int statusHoogte = 50;
  int statusBreedte = width - 40;
  fill(#FFFFFF);
  rect(20,0, statusBreedte, statusHoogte);
  fill(#000000);
  textAlign(CENTER, CENTER);
  textSize(24);
  text(status, statusBreedte/2, statusHoogte/2);

}