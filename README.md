## DrieDeurenProbleem

This application was written in my first year of a parttime ICT course at the HAN University of Applied Sciences.  
I hope you enjoy Processing (the language), because I don't.

As project for a course we had a list of game concepts to pick from. I picked this one where I had to emulate a gameshow with prizes behind doors. In the end it took a few hours to cobble this together without Processing experience.

All the texts are in Dutch because we still get programming related courses in Dutch for some reason. 


I am releasing this code because I don't think we should be ashamed of anything we write. Even if it's written in Processing by someone who'd rather use a language people would actually use in practice.



*If you would like to actually play this, you'll have to supply your own images of a goat and a car. I wanted to release the code into public domain, but I don't own the rights to the images I used*


### License

All code in this repository has been released into the public domain.  

Feel free to use it for any evil purposes, share it with your evil pals, alter it to fit your evil agenda. If you are not planning on doing anything evil with it, good on you. It applies to non-evil or neutral usage as well. Not that I can tell you what to do, it's public domain.